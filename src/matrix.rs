use crate::traits::position::MPosition;
use crate::traits::size::MSize;
use std::slice::{Iter, IterMut};
use std::ops::{Add, Sub, Mul, AddAssign};
use std::fmt::{Debug, Formatter};
use core::fmt;
use std::iter::StepBy;

#[derive(Debug)]
pub struct PositionError;
pub struct RowNumberError(&'static str);

pub struct MIter<'l, T: 'l> {
    iter: Iter<'l, T>,
    columns: usize,
    row: usize,
    col: usize
}

impl<'l, T: 'l> Iterator for MIter<'l, T> {
    type Item = (usize, usize, &'l T);

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.iter.next();
        if item.is_some() {
            let res_item =  (self.row, self.col, item.unwrap());
            self.col += 1;
            if self.col >= self.columns {
                self.row += 1;
                self.col = 0;
            }
            return Some(res_item);
        }
        return None;
    }
}

pub struct MIterMut<'l, T> {
    iter: IterMut<'l, T>,
    columns: usize,
    row: usize,
    col: usize
}

impl<'l, T> Iterator for MIterMut<'l, T> {
    type Item = (usize, usize, &'l mut T);

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.iter.next();
        if item.is_some() {
            let res_item =  (self.row, self.col, item.unwrap());
            self.col += 1;
            if self.col >= self.columns {
                self.row += 1;
                self.col = 0;
            }
            return Some(res_item);
        }
        return None;
    }
}

pub struct MIterRow<'l, T: 'l> {
    iter: Iter<'l, T>,
    row: usize,
    col: usize
}

impl<'l, T> Iterator for MIterRow<'l, T> {
    type Item = (usize, usize, &'l T);

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.iter.next();
        if item.is_none() {
            return None;
        }
        let res = Some((self.row, self.col, item.unwrap()));
        self.col += 1;
        res
    }
}

pub struct MIterRowMut<'l, T> {
    iter: IterMut<'l, T>,
    row: usize,
    col: usize
}

impl<'l, T> Iterator for MIterRowMut<'l, T> {
    type Item = (usize, usize, &'l mut T);

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.iter.next();
        if item.is_none() {
            return None;
        }
        let res = Some((self.row, self.col, item.unwrap()));
        self.col += 1;
        res
    }
}

pub struct MIterCol<'l, T> {
    iter: StepBy<Iter<'l, T>>,
    row: usize,
    col: usize
}

impl<'l, T> Iterator for MIterCol<'l, T> {
    type Item = (usize, usize, &'l T);

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.iter.next();
        if item.is_none() {
            return None;
        }
        let res = Some((self.row, self.col, item.unwrap()));
        self.row += 1;
        res
    }
}

pub struct MIterColMut<'l, T> {
    iter: StepBy<IterMut<'l, T>>,
    row: usize,
    col: usize
}

impl<'l, T> Iterator for MIterColMut<'l, T> {
    type Item = (usize, usize, &'l mut T);

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.iter.next();
        if item.is_none() {
            return None;
        }
        let res = Some((self.row, self.col, item.unwrap()));
        self.row += 1;
        res
    }
}

pub struct Matrix<T: Default + Clone + Debug> {
    data: Vec<T>,
    row_count: usize,
    col_count: usize
}

impl<T: Default + Clone + Copy + Debug> Matrix<T> {
    pub fn new< S: MSize>(size: S) -> Matrix<T> {
        Matrix {
            data: vec![Default::default(); size.rows() * size.columns()],
            row_count: size.rows(),
            col_count: size.columns()
        }
    }

    pub fn from_slice(slice: &[T]) -> Matrix<T> {
        let mut res: Matrix<T> = Matrix::new((slice.len(), 1));
        for ((_, _, setter), getter) in res.iter_mut().zip(slice.iter()) {
            *setter = *getter;
        }
        res
    }

    pub fn columns(&self) -> usize {
        self.col_count
    }

    pub fn rows(&self) -> usize {
        self.row_count
    }

    pub fn set<P: MPosition>(&mut self, pos: P, value: T) {
        if pos.column() >= self.col_count
            || pos.row() >= self.row_count {
            panic!("=={:?}==", PositionError);
        }
        let p = (self.col_count - 1) * pos.row() + pos.column() + pos.row();
        self.data[p] = value;
    }

    pub fn get<P: MPosition>(&self, pos: P) -> Result<&T, PositionError> {
        if pos.column() >= self.col_count
            || pos.row() >= self.row_count {
            return Err(PositionError);
        }
        let p = (self.col_count - 1) * pos.row() + pos.column() + pos.row();
        return Ok(unsafe { self.data.get_unchecked(p) });
    }

    pub fn get_mut<P: MPosition>(&mut self, pos: P) -> Result<&mut T, PositionError> {
        if pos.column() >= self.col_count
            || pos.row() >= self.row_count {
            return Err(PositionError);
        }
        let p = (self.col_count - 1) * pos.row() + pos.column() + pos.row();
        return Ok(unsafe { self.data.get_unchecked_mut(p) });
    }

    pub fn iter(&self) -> MIter<'_, T> {
        MIter {
            iter: self.data.iter(),
            columns: self.col_count,
            row: 0,
            col: 0
        }
    }

    pub fn iter_mut(&mut self) -> MIterMut<'_, T> {
        MIterMut {
            iter: self.data.iter_mut(),
            columns: self.col_count,
            row: 0,
            col: 0
        }
    }

    pub fn iter_row(&self, row: usize) -> MIterRow<'_, T> {
        if self.row_count <= row {
            panic!("==Error: count input row({}) mor count row matrix({})==", row +1, self.row_count);
        }
        let from: usize = (self.col_count - 1) * row + row;
        let to: usize = (self.col_count - 1) * row + self.col_count + row;
        MIterRow {
            iter: self.data[from..to].iter(),
            row,
            col: 0
        }
    }

    pub fn iter_row_mut(&mut self, row: usize) -> MIterRowMut<'_, T> {
        if self.row_count <= row {
            panic!("==Error: count input row({}) mor count row matrix({})==", row +1, self.row_count);
        }
        let from: usize = (self.col_count - 1) * row + row;
        let to: usize = (self.col_count - 1) * row + self.col_count + row;
        MIterRowMut {
            iter: self.data[from..to].iter_mut(),
            row,
            col: 0
        }
    }

    pub fn iter_col(&self, col: usize) -> MIterCol<'_, T> {
        if self.col_count <= col {
            panic!("==Error: count input column({}) mor count column matrix({})==", col +1, self.col_count);
        }
        MIterCol {
            iter: self.data[col..].iter().step_by(self.col_count),
            row: 0,
            col
        }
    }

    pub fn iter_col_mut(&mut self, col: usize) -> MIterColMut<'_, T> {
        if self.col_count <= col {
            panic!("==Error: count input column({}) mor count column matrix({})==", col +1, self.col_count);
        }
        MIterColMut {
            iter: self.data[col..].iter_mut().step_by(self.col_count),
            row: 0,
            col
        }
    }

    fn duplc_str(str: String, count: usize) -> String {
        let mut res = String::from(&str);
        for _ in 0..count {
            res.push_str(&str)
        }
        res
    }
}

impl<T: Default + ToString + Clone + Copy + Debug> fmt::Display for Matrix<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut max_len = 0usize;
        for val in self.data.iter() {
            let val_len = val.to_string().len();
            if val_len > max_len { max_len = val_len; };
        }

        write!(f, "|");
        for (x, y, val) in self.iter() {
            let str_val = val.to_string();
            let tab_count = max_len - str_val.len();

            let str = Self::duplc_str(String::from(' '), tab_count) + str_val.trim();
            write!(f, "{}", str);
            if self.row_count -1 == x && self.col_count -1 == y { write!(f, " |\n"); }
            else if y >= self.col_count -1 { write!(f, " |\n|"); }
        }
        Ok(())
    }
}

impl<T: Add<Output = T> + Default + Clone + Copy + Debug> Add for Matrix<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        if self.col_count != rhs.col_count
            || self.row_count != rhs.row_count {
            panic!("Dimensions is on equals")
        }
        let mut res = Matrix::new((self.row_count, self.col_count));

        for (a, b) in self.iter().zip(rhs.data.iter()) {
            res.set((a.0, a.1), *a.2 + *b)
        }
        res
    }
}

impl<T: Sub<Output = T> + Default + Clone + Copy + Debug> Sub for Matrix<T> {
    type Output = Matrix<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        if self.col_count != rhs.col_count
            || self.row_count != rhs.row_count {
            panic!("Dimensions is on equals")
        }
        let mut res = Matrix::new((self.row_count, self.col_count));
        for (a, b) in self.iter().zip(rhs.data.iter()) {
            res.set((a.0, a.1), *a.2 - *b)
        }
        res
    }
}

impl<T> Mul for Matrix<T> where T: Mul<Output = T> + std::default::Default + Clone + Copy + Debug + AddAssign {
    type Output = Matrix<T>;

    fn mul(self, rhs: Self) -> Self::Output {
        if self.col_count != rhs.row_count {
            panic!("==Matrix not be multiplication A col {} != B row {}==", self.col_count, rhs.row_count);
        }

        let mut res = Matrix::new((self.row_count, rhs.col_count));
        let mut c: T;

        for i in 0..self.row_count * rhs.col_count {
            c = Default::default();
            for ((_, _, a), (_, _, b)) in self.iter_row(i / rhs.col_count).zip(rhs.iter_col(i % rhs.col_count)) {
                c += (*b) * (*a);
            }
            res.set((i / rhs.col_count, i % rhs.col_count), c);
        }
        res
    }
}

// impl From<&[f32]> for Matrix<f32> {
//     fn from(data: &[f32]) -> Self {
//         let mut res = Matrix::new((data.len(), 1));
//         for ((_, _, setter), getter) in res.iter_mut().zip(data.iter()) {
//             *setter = *getter
//         }
//         res
//     }
// }

// impl From<&[f64]> for Matrix<f64> {
//     fn from(data: &[f64]) -> Self {
//         let mut res = Matrix::new((data.len(), 1));
//         for ((_, _, setter), getter) in res.iter_mut().zip(data.iter()) {
//             *setter = *getter
//         }
//         res
//     }
// }

impl From<Vec<f32>> for Matrix<f32> {
    fn from(data: Vec<f32>) -> Self {
        let mut res = Matrix::new((data.len(), 1));
        for ((_, _, setter), getter) in res.iter_mut().zip(data.iter()) {
            *setter = *getter
        }
        res
    }
}

impl From<Vec<f64>> for Matrix<f64> {
    fn from(data: Vec<f64>) -> Self {
        let mut res = Matrix::new((data.len(), 1));
        for ((_, _, setter), getter) in res.iter_mut().zip(data.iter()) {
            *setter = *getter
        }
        res
    }
}

impl<T: Default + Clone + Debug> Clone for Matrix<T> {
    fn clone(&self) -> Self {
        Self {
            data: self.data.clone(),
            row_count: self.row_count,
            col_count: self.col_count
        }
    }
}
