pub mod size {
    pub trait MSize {
        /// Return the number of rows.
        fn rows(&self) -> usize;

        /// Return the number of columns.
        fn columns(&self) -> usize;

        /// Return the number of rows and columns.
        fn dimensions(&self) -> (usize, usize) {
            (self.rows(), self.columns())
        }
    }

    impl MSize for (usize, usize) {
        fn rows(&self) -> usize {
            self.0
        }

        fn columns(&self) -> usize {
            self.1
        }
    }

    impl MSize for usize {
        fn rows(&self) -> usize {
            *self
        }

        fn columns(&self) -> usize {
            *self
        }
    }
}

pub mod position {
    pub trait MPosition {
        /// Return the number of rows.
        fn row(&self) -> usize;

        /// Return the number of columns.
        fn column(&self) -> usize;

        /// Return the number of rows and columns.
        #[inline(always)]
        fn position(&self) -> (usize, usize) {
            (self.row(), self.column())
        }
    }

    impl MPosition for (usize, usize) {
        #[inline(always)]
        fn row(&self) -> usize {
            self.0
        }

        #[inline(always)]
        fn column(&self) -> usize {
            self.1
        }
    }

    impl MPosition for usize {
        #[inline(always)]
        fn row(&self) -> usize {
            *self
        }

        #[inline(always)]
        fn column(&self) -> usize {
            *self
        }
    }
}

// pub mod item {
//     trait MItem<T> {
//         fn value(&self) -> &T;
//
//         fn value_mut(&self) -> &mut T;
//
//         fn x(&self) -> usize;
//
//         fn y(&self) -> usize;
//     }
//
//     impl<T> MItem<T> for (usize, usize, T) {
//         fn value(&self) -> &T {
//             &self.2
//         }
//
//         fn value_mut(&mut self) -> &mut T {
//             &mut self.2
//         }
//
//         fn x(&self) -> usize {
//             self.0
//         }
//
//         fn y(&self) -> usize {
//             self.1
//         }
//     }
// }