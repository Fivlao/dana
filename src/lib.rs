#[cfg(test)]
mod tests {
    use crate::matrix::{Matrix};
    #[test]
    fn it_works() {
        let mut c = 0;
        let mut a = Matrix::new((2, 7));
        a.iter_mut().for_each(|(_, _, val)| { *val = c; c += 1; });
        println!("A:\n{}\n", a);

        let mut b = Matrix::new((7, 5));
        b.iter_mut().for_each(|(_, _, val)| { *val = c; c += 1; });
        println!("B:\n{}\n", b);

        println!("A * B:\n{}", a * b);
        let s = &[3usize; 5];
        let mut v = Vec::new();
        v.push(3f32);
        let mm1 = Matrix::from_slice(s).clone();
        println!("{}", mm1);
        Vec::from(v);

        mul_matrix();
    }


    #[test]
    fn mul_matrix() {
        let mut c = Matrix::new(3);
        c.set((0, 0), 7);
        c.set((0, 1), -2);
        c.set((0, 2), 19);
        c.set((1, 0), -15);
        c.set((1, 1), 3);
        c.set((1, 2), -18);
        c.set((2, 0), 23);
        c.set((2, 1), -4);
        c.set((2, 2), 17);

        let mut a = Matrix::new((3, 2));
        a.set((0, 0), 2);
        a.set((0, 1), 1);
        a.set((1, 0), -3);
        a.set((1, 1), 0);
        a.set((2, 0), 4);
        a.set((2, 1), -1);

        println!("A:\n{}", a);

        let mut b = Matrix::new((2, 3));
        b.set((0, 0), 5);
        b.set((0, 1), -1);
        b.set((0, 2), 6);
        b.set((1, 0), -3);
        b.set((1, 1), 0);
        b.set((1, 2), 7);

        println!("B:\n{}", b);

        let c1 = a * b;

        println!("a * b =\n{}\n\n{}", c, c1);
    }

}

pub mod traits;
pub mod matrix;
